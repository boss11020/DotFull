import arcade
from time import time
from random import randint


class Ship:
    DIR_UP = 0
    DIR_RIGHT = 1
    DIR_DOWN = 2
    DIR_LEFT = 3
    DIR_STAY = 4
 
    def __init__(self, world, x, y):
        self.world = world
        self.x = x
        self.y = y
        self.direction = Ship.DIR_STAY
        
 
    def switch_direction(self, key):
        if key == arcade.key.UP:
            self.direction = Ship.DIR_UP
        elif key == arcade.key.RIGHT:
            self.direction = Ship.DIR_RIGHT
        elif key == arcade.key.DOWN:
            self.direction = Ship.DIR_DOWN
        elif key == arcade.key.LEFT:
            self.direction = Ship.DIR_LEFT
        else :
            self.direction = Ship.DIR_STAY

        
        
            
 
 
    def animate(self, delta):
        if self.direction == Ship.DIR_UP:
            if self.y > self.world.height:
                self.y = 0
            self.y += 5
        elif self.direction == Ship.DIR_RIGHT:
            if self.x > self.world.width:
                self.x = 0
            self.x += 5
        elif self.direction == Ship.DIR_DOWN:
            if self.y < 0:
                self.y = self.world.height
            self.y -= 5
        elif self.direction == Ship.DIR_LEFT:
            if self.x < 0:
                self.x = self.world.width
            self.x -= 5
        elif self.direction == Ship.DIR_STAY:
            self.x += 0

    def hit(self, other, hit_size):
        return (abs(self.x - other.x) <= hit_size) and (abs(self.y - other.y) <= hit_size)

class Enemy:

    def __init__(self, world,x,y, type_):
        self.world = world
        self.x = x
        self.y = y
        self.type_ = type_
        
    
    def animate(self, delta):
        if self.type_ == 0:
            if self.x > self.world.width:
                self.x = 0
            self.x += 5
        if self.type_ == 1:
            if self.y > self.world.height:
                self.y = 0
            self.y += 5

    

class World:


    
    def __init__(self, width, height):
        self.width = width
        self.height = height

        self.time = time()
        self.hit = False
 
        self.ship = Ship(self, 300, 300)
        self.enemys = []

    def spawn_enemy(self):
        if time() - self.time >= 0.8:
            self.enemys.append(Enemy(self,0, randint(50, 550), 0))
            self.enemys.append(Enemy(self,randint(50, 550), 0, 1))
            self.time = time()

    def on_key_press(self, key, key_modifiers):
#        if key == arcade.key.SPACE:
        self.ship.switch_direction(key)

    def on_key_release(self, key, key_modifiers):
        if key == arcade.key.UP or key == arcade.key.DOWN or LEFT or key == arcade.key.RIGHT:
            self.ship.direction = Ship.DIR_STAY
 
    def animate(self, delta):
        self.ship.animate(delta)
        self.spawn_enemy()
        for i in self.enemys:
            i.animate(delta)
            if self.ship.hit(i, 40):
                self.hit = True
            if i.x > 600 or i.y > 600:
                self.enemys.remove(i)

        #for num in range(len(self.enemys)):
         #   if int(self.enemys[num].x) > 600 or int(self.enemys[num].y) > 600:
          #      self.enemys.pop(num)
                
    

    

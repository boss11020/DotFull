import arcade


from time import time
from Model import Ship, World

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class ModelSprite(arcade.Sprite):
    def __init__(self, *args, **kwargs):
        self.model = kwargs.pop('model', None)
 
        super().__init__(*args, **kwargs)
 
    def sync_with_model(self):
        if self.model:
            self.set_position(self.model.x, self.model.y)
 
    def draw(self):
        self.sync_with_model()
        super().draw()
 
class SpaceGameWindow(arcade.Window):
    def __init__(self, width, height):
        super().__init__(width, height)
 
        arcade.set_background_color(arcade.color.BLACK)
 
        self.world = World(600,600)
        self.ship_sprite = arcade.load_texture('Images/ship.png')
        self.e = arcade.load_texture('Images/monster.png')
        self.gameover = arcade.load_texture('Images/gameover.jpg')
        
 
    def on_draw(self):

        arcade.start_render()
        arcade.draw_texture_rectangle(self.world.ship.x, self.world.ship.y,50,50, self.ship_sprite)
        for enemy in self.world.enemys:
            arcade.draw_texture_rectangle(enemy.x, enemy.y,50,50, self.e)
        if self.world.hit:
            arcade.draw_texture_rectangle(300, 300, 600, 600, self.gameover)            
        

 
    def animate(self, delta):
        self.world.animate(delta)
        
    def on_key_press(self, key, key_modifiers):
        self.world.on_key_press(key, key_modifiers)        

    
 
if __name__ == '__main__':
    window = SpaceGameWindow(SCREEN_WIDTH, SCREEN_HEIGHT)
    arcade.run()
